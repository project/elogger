# Events Logger

A module that is used for tracking all the possible drupal system
events with a variety of features around it.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/elogger).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/elogger).


### Features
Can set a system message including tokens for any type of CRUD entity
actions or form submissions. Now, all your forms submission data
can be logged and tracked for a purpose.

Once an entity performs any updates into the fields we can now track
it via a diff visualization into the logs listing which will keep us
a transparent view of who, when, and what exactly fields were changed.

Can configure which specter of modules we want to track the events,
also filter them into logs listing. Also, set the specific forms we
want to track the submissions, or just track all we do have on the
site (including custom forms).

Can specify how many logs the system may keep and the rest of the logs will
be erased by a corn job.

Can export and perform the full CRUD actions to the logs.


## Requirements/Dependencies

- [token](https://www.drupal.org/project/token)
- [views](https://www.drupal.org/project/views)
- [views_data_export](https://www.drupal.org/project/views_data_export)
- [views_bulk_operations](https://www.drupal.org/project/views_bulk_operations)
- [diff](https://www.drupal.org/project/diff)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


### Configuration pages

- Admin events logger listing: `admin/reports/elogger`
- Config page: `admin/config/system/elogger`

It has an internal drupal service for this purpose. Here are examples of usage:

```php
$elog_service = \Drupal::service('elogger.logger');

// Log an an entity event log like creation, update, delete of a Drupal entity.
// $entity: a Drupal "EntityInterface" instance like node, term, user,
// menu_link, etc.
$elog_service->setEntity($entity);
$elog_service->logEvent('entity_create');


/**
 * Implements hook_form_alter().
 */
function elogger_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Log a form submit event. It will also log the submitted form data.
  $elog_service->setForm($form, $form_state);
  $elog_service->logEvent('actions');
}
```


## Maintainers

- Andrei Vesterli - [andrei.vesterli](https://www.drupal.org/u/andreivesterli)
- Eugen Loo - [indrivoltd](https://www.drupal.org/u/indrivoltd)
- Dan Golovatic - [dan.golovatic](https://www.drupal.org/u/dangolovatic)
- Nick Murza - [nick.murza](https://www.drupal.org/u/nickmurza)
