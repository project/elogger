<?php

/**
 * @file
 * The Event logger module file.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\user\Entity\User;
use Drupal\views\ViewExecutable;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_help().
 */
function elogger_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.elogger':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('A module that is used for tracking all the possible drupal system
events with a variety of features around it.') . '</p>';
      // Add a link to the Drupal.org project.
      $output .= '<p>';
      $output .= t('Visit the <a href=":project_link">Event logger project page</a> on Drupal.org for more information.',
        [
          ':project_link' => 'https://www.drupal.org/project/elogger',
        ]
      );
      $output .= '</p>';
      return $output;
  }
}

/**
 * Implements hook_entity_insert().
 */
function elogger_entity_insert(EntityInterface $entity) {
  _log_entity_event('entity_create', $entity);
}

/**
 * Implements hook_entity_update().
 */
function elogger_entity_update(EntityInterface $entity) {
  _log_entity_event('entity_update', $entity);
}

/**
 * Implements hook_entity_delete().
 */
function elogger_entity_delete(EntityInterface $entity) {
  _log_entity_event('entity_delete', $entity);
}

/**
 * Callback for calling a core event.
 */
function _log_entity_event($event_type, $entity) {
  if (!empty($entity->__elogger_logged)) {
    // Certain entities may be called twice within the same form operation.
    // e.g. Blocks or node_form. Block it here.
    return;
  }
  $entity->__elogger_logged = TRUE;

  $elog_service = \Drupal::service('elogger.logger');
  $elog_service->setEntity($entity);
  $elog_service->logEvent($event_type);
}

/**
 * Implements hook_entity_presave().
 */
function elogger_entity_presave(EntityInterface $entity) {
  if (!$entity->isNew()) {
    // Add the entity original state before it was updated, so, we can build the
    // diff from it.
    $sid = $entity->id() . $entity->uuid();
    $session = \Drupal::service('session');
    if ($session->isStarted() && !$session->get($sid)) {
      $session->set($sid, $entity->original);
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function elogger_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $elogger_exposed_form = 'views-exposed-form-elogger-elogs';

  if (
    $form_id == 'views_exposed_form' &&
    $form['#id'] == $elogger_exposed_form
  ) {
    if (!empty($form['module'])) {
      // Convert the textfield exposed filter to a dropdown with the list of all
      // available modules (excepting "elogger").
      $options = ['' => t('All')];
      $config = \Drupal::service('config.factory')->getEditable('elogger.settings');
      $options += !empty($config->get('modules')) ? $config->get('modules') : [];

      _preprocess_exposed_filter_select($form['module'], $options);
    }

    if (!empty($form['event_type'])) {
      $options = ['' => t('All')];
      $options += \Drupal::service('elogger.logger')->getEventTypes();
      _preprocess_exposed_filter_select($form['event_type'], $options);
    }
  }

  // These forms needs to be exlucdes from the logs list due to the several
  // reasons like: login/register forms has private data like passwords and
  // admins don't need to view them. The elogger exposed form should also be
  // skipped.
  // NOTE: Need to check the commerce forms too as there might be panes where
  // the endused will enter card data and again, admin users should not be able
  // to access such information.
  $forms_to_exclude = [
    $elogger_exposed_form,
    'user_login_form',
    'user_register_form',
    'commerce_checkout_flow_multistep_default',
  ];

  // Skip the elogger exposed form from elogging events triggering.
  if (isset($form['#id']) && !in_array($form['#id'], $forms_to_exclude)) {
    // Add the form submit trigger at the start of all submit callbacks.
    if (!empty($form['#submit'])) {
      array_unshift($form['#submit'], '_add_form_submit_log');
    }
    else {
      $form['#submit'][] = '_add_form_submit_log';
    }
  }
}

/**
 * Convert the textarea exposed filter to a dropdown.
 *
 * @param array $filter
 *   The filter exposed form data.
 * @param array $options
 *   The filter dropdown options.
 */
function _preprocess_exposed_filter_select(array &$filter, array $options) {
  $filter['#type'] = 'select';
  $filter['#options'] = $options;
  $filter['#size'] = NULL;
}

/**
 * Elogger form submit global handler.
 */
function _add_form_submit_log(&$form, FormStateInterface $form_state) {
  // Log an event once a form was submitted.
  $elog_service = \Drupal::service('elogger.logger');
  $elog_service->setForm($form, $form_state);
  $elog_service->logEvent('actions');
}

/**
 * Implements hook_views_pre_render().
 */
function elogger_views_pre_render(ViewExecutable $view) {
  if ($view->id() == 'elogger') {
    $elog_service = \Drupal::service('elogger.logger');
    foreach ($view->result as $value) {
      /** @var \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $value->_entity;

      $diff = $entity->diff->value;
      if (!empty($diff)) {
        $diff = unserialize($diff, ['allowed_classes' => [TranslatableMarkup::class]]);
        $table_diff = $elog_service->buildDiffOutput($diff);
        // Display the system diff theme output data table for this entity.
        $entity->set('diff', $table_diff);
      }

      $form_data = $entity->form_data->value;
      if (!empty($form_data)) {
        $form_data = unserialize($form_data, ['allowed_classes' => [TranslatableMarkup::class]]);
        $formatted_data = $elog_service->buildFormData($form_data);
        // Display the formatted form data as a json output pretty format.
        $entity->set('form_data', $formatted_data);
      }
    }
  }
}

/**
 * Implements hook_cron().
 */
function elogger_cron() {
  // Cleanup the watchdog table.
  $row_limit = \Drupal::config('elogger.settings')->get('elogger_row_limit');

  if ($row_limit > 0) {
    $connection = \Drupal::database();
    $min_row = $connection->select('elog', 'elog')
      ->fields('elog', ['id'])
      ->orderBy('id', 'DESC')
      ->range($row_limit - 1, 1)
      ->execute()->fetchField();

    if ($min_row) {
      $connection->delete('elog')
        ->condition('id', $min_row, '<')
        ->execute();
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function elogger_form_system_logging_settings_alter(array &$form, FormStateInterface $form_state) {
  $config = \Drupal::configFactory()->getEditable('elogger.settings');
  if (\Drupal::moduleHandler()->moduleExists('syslog')) {
    $form['elogger_format'] = [
      '#type' => 'textarea',
      '#title' => t('Elogger format'),
      '#default_value' => $config->get('format'),
      '#weight' => 10,
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['elogger_format_tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['elogger'],
        '#global_types' => TRUE,
        '#click_insert' => TRUE,
        '#weight' => 11,
      ];
    }

    $form['elogger_output_type'] = [
      '#type' => 'select',
      '#title' => t('Elogger Output type'),
      '#default_value' => $config->get('output_type'),
      '#options' => [
        'watchdog' => t('Watchdog (default)'),
        'syslog' => t('Raw Syslog'),
      ],
      '#description' => t('<strong>Note:</strong> The Raw Syslog option dumps output <em>exactly</em> as in ELT format above, so you need to add your own timestamp, IP, etc using tokens.<br/>The Watchdog (default) option dumps output using the Syslog format with "message" field formatted using the ELT format above.'),
      '#weight' => 12,
    ];

    $row = [
      'event_type' => t('Event Type example'),
      'module' => t('Elogger'),
      'log_message' => 'Example of syslog message',
      'created' => \Drupal::time()->getRequestTime(),
      'changed' => \Drupal::time()->getRequestTime() + 1,
      'entity' => User::load(1),
      'user' => 1,
    ];
    $token_service = \Drupal::token();
    $bubbleable_metadata = new BubbleableMetadata();
    $output = $token_service->replace($config->get('format'), ['elogger' => $row], [], $bubbleable_metadata);

    $form['example'] = [
      '#type' => 'textarea',
      '#title' => t('Example output based on above'),
      '#default_value' => $output,
      '#disabled' => TRUE,
      '#weight' => 12,
    ];

    $form['#submit'][] = 'elogger_logging_settings_submit';
  }
}

/**
 * Logging settings page submit hook.
 *
 * @throws Drupal\Core\Config\ConfigValueException
 */
function elogger_logging_settings_submit($form, FormStateInterface $form_state) {
  if (\Drupal::moduleHandler()->moduleExists('token')) {
    \Drupal::configFactory()->getEditable('elogger.settings')
      ->set('format', $form_state->getValue('elogger_format'))
      ->set('output_type', $form_state->getValue('elogger_output_type'))
      ->save();
  }
}

/**
 * Implements hook_token_info().
 */
function elogger_token_info(): array {
  $type = [
    'name' => t('Elogger'),
    'description' => t('Tokens related to elogger'),
    'needs-data' => 'event-log',
  ];

  $event['log_message'] = [
    'name' => t('Log Message Value'),
  ];
  $event['event_type'] = [
    'name' => t('Event Type'),
  ];
  $event['module'] = [
    'name' => t('Module'),
  ];
  $event['user'] = [
    'name' => t('User who made the action'),
    'type' => 'user',
  ];
  $event['created'] = [
    'name' => t('Date action was taken'),
    'type' => 'date',
  ];
  $event['changed'] = [
    'name' => t('Changed date'),
    'type' => 'date',
  ];

  return [
    'types' => ['elogger' => $type],
    'tokens' => ['elogger' => $event],
  ];
}

/**
 * Implements hook_tokens().
 */
function elogger_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $token_service = \Drupal::token();

  $langcode = $options['langcode'] ?? LanguageInterface::LANGCODE_DEFAULT;

  $replacements = [];
  if ($type === 'elogger' && !empty($data['elogger'])) {
    $event = $data['elogger'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'log_message':
          $replacements[$original] = $event['log_message']['value'] ?? $event['log_message'];
          break;

        case 'event_type':
          $replacements[$original] = $event['event_type'];
          break;

        case 'module':
          $replacements[$original] = $event['module'];
          break;

        case 'user':
          if (isset($event['user'])) {
            $account = User::load($event['user']);
            $bubbleable_metadata->addCacheableDependency($account);
            $replacements[$original] = $account->label();
          }
          else {
            $replacements[$original] = 'System';
          }
          break;

        case 'created':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')
            ->format($event['created'], $date_format->id(), '', NULL, $langcode);
          break;

        case 'changed':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')
            ->format($event['changed'], $date_format->id(), '', NULL, $langcode);
          break;
      }
    }

    if ($author_tokens = $token_service->findWithPrefix($tokens, 'user')) {
      $account = User::load($event['user']);
      $replacements += $token_service->generate('user', $author_tokens, ['user' => $account], $options, $bubbleable_metadata);
    }

    if ($created_tokens = $token_service->findWithPrefix($tokens, 'created')) {
      $replacements += $token_service->generate('date', $created_tokens, ['date' => $event['created']], $options, $bubbleable_metadata);
    }

    if ($changed_tokens = $token_service->findWithPrefix($tokens, 'changed')) {
      $replacements += $token_service->generate('date', $changed_tokens, ['date' => $event['changed']], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
