<?php

namespace Drupal\elogger\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\elogger\Services\Elogger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure ElogLogMessagesSettingsForm form.
 */
class ElogLogMessagesSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'elogger.settings';

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The event logger service.
   *
   * @var \Drupal\elogger\Services\Elogger
   */
  protected Elogger $elogger;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Constructs a new ElogLogMessagesSettingsForm.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\elogger\Services\Elogger $elogger
   *   The event logger service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    Elogger $elogger,
    RendererInterface $renderer
  ) {
    $this->moduleHandler = $module_handler;
    $this->elogger = $elogger;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('elogger.logger'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'elog_log_messages_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form = parent::buildForm($form, $form_state);

    $row_limits = [100, 1000, 10000, 100000, 1000000];
    $form['elogger_row_limit'] = [
      '#type' => 'select',
      '#title' => $this->t('Database log messages to keep'),
      '#default_value' => $config->get('elogger_row_limit'),
      '#options' => [0 => $this->t('All')] + array_combine($row_limits, $row_limits),
      '#description' => $this->t('The maximum number of messages to keep in the elog table. Requires a <a href=":cron">cron maintenance task</a>.', [':cron' => Url::fromRoute('system.status')->toString()]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('elogger_row_limit', $form_state->getValue('elogger_row_limit'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
