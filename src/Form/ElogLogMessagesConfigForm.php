<?php

namespace Drupal\elogger\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\elogger\Services\Elogger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure ElogLogMessagesConfigForm form.
 */
class ElogLogMessagesConfigForm extends ConfigFormBase {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'elogger.settings';

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The event logger service.
   *
   * @var \Drupal\elogger\Services\Elogger
   */
  protected Elogger $elogger;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new ElogLogMessagesConfigForm.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\elogger\Services\Elogger $elogger
   *   The event logger service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    Elogger $elogger,
    RendererInterface $renderer,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->moduleHandler = $module_handler;
    $this->elogger = $elogger;
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('elogger.logger'),
      $container->get('renderer'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'elog_log_messages_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form = parent::buildForm($form, $form_state);

    $log_message_templates = $config->get('log_message_templates');
    // Get all possible event types.
    $event_types = $this->elogger->getEventTypes();

    $form['wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Customize message templates'),
      '#collapsible' => FALSE,
    ];

    // Display a cofigurable textarea for each log message event type in part.
    foreach ($event_types as $type => $type_label) {
      $tokens = [
        '{entity_type}: The entity type was affected. For ex: file, node, user, term etc.',
        '{entity}: The entity label or (if possible), the view url. For ex: the user view url, or node url.',
      ];

      if ($type == 'actions') {
        $tokens = ['{form_id}: The submitted form id string.'];
      }
      $tokens[] = '{user}: The user name linked to his profile.';

      // Build the built-in tokens list and render them.
      $tokens_list = ['#theme' => 'item_list', '#items' => $tokens];
      $build_in_tokens = $this->renderer->render($tokens_list);

      $form['wrapper'][$type] = [
        '#type' => 'textarea',
        '#title' => $type_label,
        '#default_value' => !empty($log_message_templates[$type]) ? $log_message_templates[$type] : '',
        '#required' => TRUE,
        '#description' => $this->t(
          'You can also use these built-in tokens:@build_in_tokens.',
          ['@build_in_tokens' => $build_in_tokens]
        ),
      ];
      $form['wrapper']["token_tree_$type"] = [
        '#theme' => 'token_tree_link',
        '#show_restricted' => TRUE,
        '#show_nested' => TRUE,
      ];
    }

    $options = [];
    try {
      $formats = $this->entityTypeManager->getStorage('filter_format')->loadMultiple();
      foreach ($formats as $format) {
        $options[$format->id()] = $format->label();
      }
    }
    catch (\Exception $e) {
      $this->getLogger('elogger')->error('An error occurred while loading text formats: @message', ['@message' => $e->getMessage()]);
    }

    $default_format = $config->get('elogger_text_format') ?: 'full_html';

    $form['elogger_text_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Text format for log messages'),
      '#options' => $options,
      '#default_value' => $default_format,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $log_message_templates = [];
    foreach (array_keys($this->elogger->getEventTypes()) as $type) {
      $log_message_templates[$type] = $form_state->getValue($type);
    }
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('log_message_templates', $log_message_templates)
      ->set('elogger_text_format', $form_state->getValue('elogger_text_format'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
