<?php

namespace Drupal\elogger\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Elog entity.
 *
 * @ingroup elogger
 */
interface ElogInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {}
