(function ($, Drupal) {

  'use strict';

  // Add a pretty formatte for the submitted forms.
  Drupal.behaviors.EloggerJsonPanel = {
    attach: function (context, settings) {

      let jsonPanelElements = $('.jsonpanel', context);

      if (jsonPanelElements.length) {
        $(once('EloggerJsonPanel', jsonPanelElements)).each((event, element) => {
          let hash = $(element).data('hash');
          $(element).jsonpanel({ data: settings.jsonpanel[hash] });
        })
      }
    }
  };

})(jQuery, Drupal);
